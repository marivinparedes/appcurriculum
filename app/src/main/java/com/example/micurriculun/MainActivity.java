package com.example.micurriculun;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
Button estudios;
Button habilidad;
Button conocimiento;
Button referencia;
Button salir;
Button datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        estudios= findViewById(R.id.boton_estudios);
        habilidad=findViewById(R.id.boton__habilidades);
        conocimiento=findViewById(R.id.boton_conocimiento);
        referencia=findViewById(R.id.boton_referencia);
        salir=findViewById(R.id.button_SALIR);
        datos=findViewById(R.id.button_datos);

        estudios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this,Estudioss.class);
                startActivity(intent);
            }
        });
        habilidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this,habilidades.class);
                startActivity(intent);
            }
        });
        conocimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this,reconocimientos.class);
                startActivity(intent);
            }
        });
        referencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this,referencias.class);
                startActivity(intent);
            }
        });
        datos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Datospersonales.class);
                startActivity(intent);
            }
        });
        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

}
